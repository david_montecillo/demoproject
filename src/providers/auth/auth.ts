import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

export class LoginCredentials{
  email: string;
  password: string;

  constructor(email: string, password: string){
    this.email = email;
    this.password = password;
  }

}

@Injectable()
export class AuthProvider {
  currentLoginCredentials: LoginCredentials;


  constructor(public http: Http) {
    console.log('Hello AuthProvider Provider');
  }

  login(loginCredentials){
    var patt= new RegExp('^[a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,15})$');
    if (loginCredentials.email === ''){
        return Observable.throw("Email is required.");
    }else if (loginCredentials.password === ''){
        return Observable.throw("Password is required.");
    }else if (!patt.test(loginCredentials.email)){
        return Observable.throw("Not an email address.");
    }else if (loginCredentials.password.length < 4){
        return Observable.throw("Password Max-Length must be atleast 4.");
    }else {
      return Observable.create(observer => {

        this.currentLoginCredentials = new LoginCredentials(loginCredentials.email, loginCredentials.password);
        observer.next(true);
        observer.complete();
      });
    }

  }

}
