import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the StorageServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class StorageServiceProvider {

  projects: Array<{title: string}>;
  constructor(public storage: Storage) {
    this.projects = new Array<{title: string}>();
    // this.storage.forEach((value) => {
    //     this.projects.push({
    //       title: value
    //     });
    // });
  }

  createNewProject(newProject){

    if (newProject.name === null || newProject.name === ''){
      return Observable.throw('Project name must be filled up.');
    }

    return Observable.create(observer => {

      this.storage.get(newProject.name).then((data)=> {
        // asynchronously return data
        if (data === null){
          this.storage.set(newProject.name, newProject.name);
          observer.next(true);
        }else{
          observer.next(false);
        }

        observer.complete();
      });
    });

  }

  lists(){
    this.projects = [];
    this.storage.forEach((value) => {
        this.projects.push({
          title: value
        });
    });
    return this.projects;
  }


}
