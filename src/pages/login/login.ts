import { Component } from '@angular/core';
import { IonicPage, NavController , AlertController, LoadingController, Loading, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ProjectsListPage } from '../projects-list/projects-list';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
loading: Loading;
loginCredentials = { email: '', password: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams , private auth: AuthProvider , private alertCtrl: AlertController , private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    this.showLoading();
    console.log(this.loginCredentials.email + " " + this.loginCredentials.password);
    this.auth.login(this.loginCredentials).subscribe(allowed => {
      if (allowed){
        this.loginCredentials.email = '';
        this.loginCredentials.password = '';
        // TODO : ADD SETROOT PROJECT SCREEN
        this.navCtrl.setRoot(ProjectsListPage);
        console.log('Access Granted')
      }else {
        console.log('Access Denied');
      }

    } , error => {
      this.showError(error);
    });


  }

  showLoading() {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: true
      });
      this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['CLOSE']
    });
    alert.present(prompt);
  }

}
