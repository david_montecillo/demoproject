import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { Component  } from '@angular/core';
import { By } from '@angular/platform-browser';
import { IonicPage, NavController, IonicPageModule ,IonicModule } from 'ionic-angular';
import { IonicStorageModule , Storage  } from '@ionic/storage';
import { StorageServiceProvider} from '../../providers/storage-service/storage-service';
import { ProjectsListPage } from './projects-list';
import { MyApp } from '../../app/app.component';
import { DebugElement } from '@angular/core';

export function provideStorage() {
 return new Storage();
}

let comp: ProjectsListPage;
let fixture: ComponentFixture<ProjectsListPage>;
let de: DebugElement;
let el: HTMLElement;

describe('Page: ProjectListPage', () => {

    beforeEach(async(() => {

        TestBed.configureTestingModule({

            declarations: [MyApp, ProjectsListPage],

            providers: [
              NavController,
              StorageServiceProvider,
              { provide: Storage, useFactory: provideStorage }
            ],

            imports: [
                IonicModule.forRoot(MyApp)
            ]

        }).compileComponents();

    }));

    beforeEach(() => {

        fixture = TestBed.createComponent(ProjectsListPage);
        comp    = fixture.componentInstance;

    });

    afterEach(() => {
        fixture.destroy();
        comp = null;
        de = null;
        el = null;
    });

    it('is created', () => {

        expect(fixture).toBeTruthy();
        expect(comp).toBeTruthy();
    });

    it('initialises with a title of Projects', () => {
        expect(comp['title']).toEqual('Projects');
    });

    it('initialise with an array' , () => {
         expect(Array.isArray(comp['projects'])).toBeTruthy;
    });

    it('initialise with an ion-list' , () => {
        de = fixture.debugElement.query(By.css('ion-list'));
        el = de.nativeElement;
        expect(el).toBeTruthy;
    });

    it('can add new project' , () => {
        comp.onNewProject();
        fixture.detectChanges();
    });

});
