import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {CreateProjectPage } from '../create-project/create-project';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
/**
 * Generated class for the ProjectsListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-projects-list',
  templateUrl: 'projects-list.html',

})
export class ProjectsListPage {
  title: string = "Projects";
  projects: Array<{title: string}>;


  constructor(public navCtrl: NavController, private storage: StorageServiceProvider) {
    this.projects = [];
  }
  ionViewWillEnter(){
    console.log('ionViewWillEnter ProjectsListPage');
    this.projects = [];
    this.projects = this.storage.lists();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectsListPage');
  }

  onClick(event, project) {
    console.log('onClick');
  }

  onNewProject(){
    this.navCtrl.push(CreateProjectPage);
    console.log('NewProject');
  }

  changeTitle(newTitle){
    this.title = newTitle;
  }

}
