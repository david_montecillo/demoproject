import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController , ToastController  } from 'ionic-angular';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';
/**
 * Generated class for the CreateProjectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-create-project',
  templateUrl: 'create-project.html',
})
export class CreateProjectPage {

  newProject = {name : ''};

  constructor(public navCtrl: NavController, public navParams: NavParams , public storage: StorageServiceProvider , public alertCtrl: AlertController , public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProjectPage');
  }

  createProject(){
    this.storage.createNewProject(this.newProject).subscribe(allowed => {

      if (allowed){
        this.newProject.name = ''
        console.log('Project Created' + this.newProject.name );
        this.showSuccess('Project Created Successfully!');
      }else {
        console.log('Not allowed project already existed! ' + this.newProject.name );
        this.showError('Project Already existed!');
      }
    }, error => {
      console.log(error);
      this.showError(error);
    });

  }


  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['CLOSE']
    });
    alert.present(prompt);
  }

  showSuccess(text) {

    const toast = this.toastCtrl.create({
      message: text,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }


}
